/**
 * Práctica 7 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

public class MarmitaVikinga {
    
    private int marmita;

    public MarmitaVikinga(int nAnguilas) {
        this.marmita=nAnguilas;
    }

    public synchronized void comer() {
        while (marmita==0) {
            notifyAll();
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        marmita--;
    }

    public synchronized void cocinar() {
        while (marmita>0) {
            try {
                wait();
            } catch (InterruptedException e) {}
        }
        marmita=10;
        notifyAll();
    }
}
