/**
 * Práctica 7 - PCTR
 * 2017/18
 *
 * @author Iván Magariño Aguilar
 */

public class UsaMarmitaVikinga implements Runnable {

    private int tipoVikingo;
    private int m = 10;

    public UsaMarmitaVikinga (MarmitaVikinga m, int typeViking) {
        this.tipoVikingo=typeViking;
        this.m=m;
    }

    public void run {
        switch (tipoVikingo) {
            case 0: while (true) {
                try {
                    this.sleep(100);
                } catch (InterruptedException e) {}
                m.comer();
                System.out.println("Comiendo...");
            }
            case 1: while (true) {
                try {
                    this.sleep(100);
                } catch (InterruptedException e) {}
                m.cocinar();
                System.out.println("Cocinando...");
            }
        }
    }

    public static void main(String[] args) {
        MarmitaVikinga marmita = new MarmitaVikinga(10);
        UsaMarmitaVikinga vikingo1, vikingo2;
        vikingo1 = new UsaMarmitaVikinga(marmita, 0);
        vikingo2 = new UsaMarmitaVikinga(marmita, 0);
        vikingo1.start();
        vikingo2.start();
    }
}
